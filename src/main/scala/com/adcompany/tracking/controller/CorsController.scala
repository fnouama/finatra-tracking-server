package com.adcompany.tracking.controller

import com.twitter.finagle.http.Request
import com.twitter.finatra.http.Controller

/**
  * Created by francknouama on 12/29/16.
  */
class CorsController extends Controller {
  options("/api/:*") {
    _: Request => response.ok
  }
}
