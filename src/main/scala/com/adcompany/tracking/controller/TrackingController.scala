package com.adcompany.tracking.controller

import java.util.UUID.randomUUID

import com.adcompany.tracking.service.EventSenderService
import com.twitter.finagle.http.{Cookie, Request}
import com.twitter.finatra.http.Controller

/**
  * Created by francknouama on 12/28/16.
  */
class TrackingController(eventProducerService: EventSenderService) extends Controller {

  get("/api/trace") { request: Request =>

    var userId: String = ""
    var isNewCookie = false
    var visitedPageURL = ""

    request.cookies.get("sw_user_id") match {
      case None =>
        userId = randomUUID().toString
        isNewCookie = true

      case Some(c) =>
        userId = c.value
    }

    request.params.get("page_visited") match {
      case None =>
        throw response.badRequest("The query parameter `page_visited` is mandatory").toException
      case Some(v) =>
        visitedPageURL = v
    }

    eventProducerService.emitEventFor(userId, visitedPageURL) onSuccess { _ =>

      if (isNewCookie) {
        val userIDCookie = new Cookie(name = "sw_user_id", value = userId)
        // TODO: set max age for cookie
        //        userIDCookie.maxAge = 20 * 365
        response.accepted.cookie(userIDCookie)
      } else {
        response.accepted
      }
    } onFailure { t: Throwable =>

      response.internalServerError(Map("error" -> t.getMessage))
    }

  }
}
