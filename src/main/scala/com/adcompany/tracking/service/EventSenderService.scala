package com.adcompany.tracking.service

import com.twitter.util.Future

/**
  * Created by francknouama on 12/29/16.
  */
trait EventSenderService {

  def emitEventFor(userId: String, pixelURL: String): Future[Unit]
}
