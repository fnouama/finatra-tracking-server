package com.adcompany.tracking.service

import com.adcompany.tracking.client.KafkaProducer
import com.adcompany.tracking.model.Visit
import com.fasterxml.jackson.databind.ObjectMapper
import com.twitter.finatra.utils.FuturePools
import com.twitter.inject.Logging
import com.twitter.util.Future
import org.joda.time.{DateTime, DateTimeZone}

/**
  * Created by francknouama on 12/29/16.
  */
class KafkaEventSenderService(kafkaProducer: KafkaProducer) extends EventSenderService with Logging {

  private val futurePool = FuturePools.unboundedPool("CallbackKafkaProducer")

  private val objectMapper = new ObjectMapper()

  override def emitEventFor(userId: String,
                            pixelURL: String): Future[Unit] = {
    futurePool {
      kafkaProducer.send(
        objectMapper.writeValueAsString(
          Visit(userId, pixelURL, DateTime.now(DateTimeZone.UTC)
          )
        )
      )
    }
  }
}




