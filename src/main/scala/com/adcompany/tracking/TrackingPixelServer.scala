package com.adcompany.tracking

import com.adcompany.tracking.client.KafkaProducer
import com.adcompany.tracking.controller.{CorsController, TrackingController}
import com.adcompany.tracking.service.KafkaEventSenderService
import com.twitter.finagle.http.filter.Cors
import com.twitter.finagle.http.filter.Cors.HttpFilter
import com.twitter.finatra.http.HttpServer
import com.twitter.finatra.http.filters.CommonFilters
import com.twitter.finatra.http.routing.HttpRouter

/**
  * Created by francknouama on 12/28/16.
  */
object TrackingPixelServerMain extends TrackingPixelServer

class TrackingPixelServer extends HttpServer {

  override def disableAdminHttpServer = true

  override def defaultFinatraHttpPort = ":8080"

  override protected def configureHttp(router: HttpRouter): Unit = {
    val config = ConfigFactory.load()


    val kafkaProducer = KafkaProducer()
    val kafkaService = new KafkaEventSenderService(kafkaProducer)
    val trackingController = new TrackingController(kafkaService)
    router
      .filter[CommonFilters]
      .filter(new HttpFilter(Cors.UnsafePermissivePolicy))
      .add[CorsController]
      .add(trackingController)
  }
}
