package com.adcompany.tracking.model

import com.fasterxml.jackson.annotation.{JsonCreator, JsonProperty}
import org.joda.time.DateTime

/**
  * Created by francknouama on 12/29/16.
  */
@JsonCreator
case class Visit(@JsonProperty("user_id") userId: String,
                 @JsonProperty("pixel_url") pixelURL: String,
                 @JsonProperty("visit_dttm") visitDttm: DateTime)
